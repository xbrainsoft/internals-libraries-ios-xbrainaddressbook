//
//  HowToXBrainAddressbook-Bridging-Header.h
//  HowToXBrainAddressbook
//
//  Created by Christophe Cadix on 11/01/2016.
//  Copyright © 2015 xBrainSoft. All rights reserved.
//

#ifndef HowToXBrainAddressbook_Bridging_Header_h
#define HowToXBrainAddressbook_Bridging_Header_h

#import "XBrainAddressbook.h"
#import "XBrainClient.h"

#endif /* HowToXBrainAddressbook_Bridging_Header_h */
