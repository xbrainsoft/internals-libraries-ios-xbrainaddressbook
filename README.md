# XBrainAddressbook (latest: 1.4.0)
In this repository you will find:

- the XBrainAddressbook iOS framework available as a Pod (CocoaPods)
- along with a sample iOS app project

## Framework
The actual framework is located there: `XBrainAddressbook/XBrainAddressbook.framework`.

## HowToXBrainAddressbook
A sample app showcasing the basic use of the *XBrainAddressbook* framework is located in the `HowToXBrainAddressbook/` folder.

## CocoaPods
This pod's `XBrainAddressbook.podspec` file is provided at the root folder level so that this pod may be used by directly referencing it from here:
```ruby
pod 'XBrainAddressbook', :git => 'https://bitbucket.org/xbrainsoft/internals-libraries-ios-xbrainaddressbook.git', :tag => '1.4.0'
```
Note that the `tag` attribute is optional here.

It may also be found in the Specs repository:  
https://bitbucket.org/xbrainsoft/internals-libraries-ios-podspecs.git

As a result you may otherwise use this pod by simply referencing it this way in your `Podfile`:  
```ruby
pod 'XBrainAddressbook', '~> 1.4.0'
```
Note that the version constraint is optional here.

The specs repositories must be added though in the `Podfile`:
```ruby
source 'https://bitbucket.org/xbrainsoft/internals-libraries-ios-podspecs.git'
source 'https://github.com/CocoaPods/Specs.git'
...
pod 'XBrainAddressbook'
...
```
(The CocoaPods specs are implicitly added until your add your own, thus we must add them)