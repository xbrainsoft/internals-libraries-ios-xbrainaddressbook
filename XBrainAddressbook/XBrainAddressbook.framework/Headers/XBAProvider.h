//
//  XBAProvider.h
//  XBrainAddressbook
//
//  Created by xBrainsoft on 19/06/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Defines a synchronization provider
 */
@protocol XBAProvider <NSObject>

/**
 * Get the provider's name
 */
- (nonnull NSString*) name;

/**
 * Get an array of dictionaries. Each dictionary represents one contact.
 */
- (nullable NSArray<NSDictionary<NSString *, id> *> *) getContactData;

@end
