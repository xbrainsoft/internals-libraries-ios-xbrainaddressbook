//
//  XBAAddressbook.h
//  XBrainAddressbook
//
//  Created by xBrainsoft on 19/06/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XBAProvider.h"

#pragma mark - XBAAddressbookDelegate

@class XBAAddressbook;

NS_ASSUME_NONNULL_BEGIN
/**
 * Keeps track of the synchronization's execution status
 */
@protocol XBAAddressbookDelegate <NSObject>

/**
 *  Synchronization has started
 *
 *  @param addressbook The <i>XBAAddressbook</i> instance
 */
- (void)onSynchronizationStart:(XBAAddressbook*)addressbook;
/**
 *  Synchronization has complete
 *
 *  @param addressbook The <i>XBAAddressbook</i> instance
 */
- (void)onSynchronizationComplete:(XBAAddressbook*)addressbook;
/**
 *  Synchronization has failed
 *
 *  @param addressbook The <i>XBAAddressbook</i> instance
 */
- (void)onSynchronizationFailed:(XBAAddressbook*)addressbook;

@end

#pragma mark - XBAAddressbook
/**
 * Main entry point of the framework. Used as a singleton.
 */
@interface XBAAddressbook : NSObject

/**
 * Shared instance
 */
+ (XBAAddressbook *)sharedInstance;

/**
 * Returns the current version number
 */
+ (NSString*) versionNumber;

/**
 * Returns the current build number
 */
+ (NSString *)buildNumber;

/**
 * Is the synchronization process running ?
 */
+ (BOOL) isSynchronizationRunning;

/**
 * Set the <i>XBAAddressbookDelegate</i> enabling to keep track of the synchronization's execution status
 * @param delegate a <i>XBAAddressbookDelegate</i>
 */
+ (void) setDelegate:(id<XBAAddressbookDelegate>)delegate;

#pragma mark Providers
/**
 * Get all the providers
 */
+ (NSArray<id<XBAProvider>> *) providers;

/**
 * Add a provider
 * @param provider a <i>XBAProvider</i>
 */
+ (void) addProvider:(id<XBAProvider>)provider;

/**
 * Remove a provider
 * @param provider a <i>XBAProvider</i>
 */
+ (void) removeProvider:(id<XBAProvider>)provider;

#pragma mark Sync
/**
 * Request a synchronization be started. It will start if and only if it is not already runnning.
 */
+ (void) enableSynchronization;

/**
 * Get the last time a synchronization finished
 */
+ (nullable NSDate*) lastSynchronizationDate;

@end
NS_ASSUME_NONNULL_END
