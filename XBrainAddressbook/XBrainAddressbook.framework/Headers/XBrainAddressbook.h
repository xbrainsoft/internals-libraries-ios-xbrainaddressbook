//
//  XBrainAddressbook.h
//  XBrainAddressbook
//
//  Created by xBrainsoft on 21/05/15.
//  Copyright (c) 2015 xbrain.io. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for XBrainAddressbook.
FOUNDATION_EXPORT double XBrainAddressbookVersionNumber;

//! Project version string for XBrainAddressbook.
FOUNDATION_EXPORT const unsigned char XBrainAddressbookVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <XBrainAddressbook/PublicHeader.h>
#import <XBrainAddressbook/XBAAddressbook.h>
#import <XBrainAddressbook/XBAProvider.h>

