Pod::Spec.new do |s|  
  s.name         = "XBrainAddressbook"
  s.version      = "1.6.2"
  s.summary      = "XBrain addressbook handling library"
  s.homepage     = "http://xbrain.io"
  s.license      = ''
  s.author       = { "Nicolas Mauri" => "nicolas.mauri@xbrain.io" }
  s.platform     = :ios, '8.0'
  s.requires_arc = true

  #### uncomment in development mode (i.e. pod locally referenced)
  #### comment out otherwise
  #s.source_files  = 'XBrainAddressbook', 'XBrainAddressbook/**/*.{h,m}'
  #s.public_header_files = 'XBrainAddressbook/**/*.h'
  ####
  
  #### comment in development mode (i.e. pod locally referenced)
  #### uncomment otherwise
  s.source_files = 'XBrainAddressbook/XBrainAddressbook.framework/Headers/*.h'
  s.vendored_frameworks = 'XBrainAddressbook/XBrainAddressbook.framework'
  ####

  s.source = { :git => "https://bitbucket.org/xbrainsoft/internals-libraries-ios-xbrainaddressbook.git", :tag => s.version }

  s.dependency 'XBrainClient', '~> 1.4'
  s.dependency 'AFNetworking', '~> 2.0'
  s.library = 'sqlite3'

end  
